require 'csv'

require_relative 'ecommerce/model'
require_relative 'ecommerce/coupon'
require_relative 'ecommerce/order'
require_relative 'ecommerce/order_item'
require_relative 'ecommerce/product'

class Ecommerce
  class << self

    def process!
      load_data!
      process_orders!
      write_output!
    end

    private

    def load_data!
      @coupons_path      = ARGV[0]
      @products_path     = ARGV[1]
      @orders_path       = ARGV[2]
      @orders_items_path = ARGV[3]
      @output_path       = ARGV[4]

      Coupon.load_data    @coupons_path
      Product.load_data   @products_path
      Order.load_data     @orders_path
      OrderItem.load_data @orders_items_path
    end

    def process_orders!
      Order.all.each(&:process!)
    end

    def write_output!
      CSV.open(@output_path, 'wb') do |csv|
        Order.all.each do |order|
          csv << [order.id, order.total]
        end
      end
    end

  end
end
