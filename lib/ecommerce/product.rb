class Ecommerce
  class Product < Model

    attr_reader :id,
                :price

    def initialize(id, price)
      @id     = id.to_i
      @price  = price.to_f
    end

  end
end
