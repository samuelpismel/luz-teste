class Ecommerce
  class OrderItem < Model

    attr_reader :order_id,
                :product_id

    def initialize(order_id, product_id)
      @order_id   = order_id.to_i
      @product_id = product_id.to_i
    end

    def product
      Product.all.find{ |product| product.id == @product_id }
    end

  end
end
