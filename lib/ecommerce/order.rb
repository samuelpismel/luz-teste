class Ecommerce
  class Order < Model

    attr_reader :id,
                :coupon_id,
                :coupon,
                :items,
                :products,
                :sub_total,
                :discount,
                :total

    def initialize(id, coupon_id)
      @id         = id.to_i
      @coupon_id  = coupon_id.to_i
      @items      = []
      @products   = []
      @sub_total  = 0
      @discount   = 0
      @total      = 0
    end

    def process!
      load_coupon
      load_items
      load_products
      sum_subtotal
      apply_discount
      sum_total
    end

    private

    def load_coupon
      @coupon = Coupon.all.find{ |coupon| coupon.id == @coupon_id }
    end

    def load_items
      @items = OrderItem.all.select{ |item| item.order_id == @id }
    end

    def load_products
      @items.each do |item|
        @products << item.product
      end
    end

    def sum_subtotal
      @products.each do |product|
        @sub_total += product.price
      end
    end

    def calculate_coupon_discount
      discount = 0
      if @coupon && @coupon.active?
        case @coupon.discount_type
        when 'absolute'
          discount = @coupon.discount
        when 'percent'
          percentage = @coupon.discount / 100
          discount = @sub_total * percentage
        end
      end
      discount
    end

    def calculate_progressive_discount
      percentage = 0
      products_count = @products.size
      if products_count >= 2
        percentage = 0.1
        percentage += ((products_count - 2) * 0.05)
        percentage = 0.4 if products_count >= 8
      end
      discount = @sub_total * percentage
      discount
    end

    def apply_discount
      coupon_discount      = calculate_coupon_discount
      progressive_discount = calculate_progressive_discount

      if coupon_discount > progressive_discount
        @coupon.increment_uses!
        @discount = coupon_discount
      else
        @discount = progressive_discount
      end
    end

    def sum_total
      @total = @sub_total - @discount
    end

  end
end
