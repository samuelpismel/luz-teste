class Ecommerce
  class Coupon < Model

    attr_reader :id,
                :discount,
                :discount_type,
                :expiration_date,
                :max_uses,
                :uses

    def initialize(id, discount, discount_type, expiration_date, max_uses)
      @id               = id.to_i
      @discount         = discount.to_f
      @discount_type    = discount_type
      @expiration_date  = Date.parse(expiration_date)
      @max_uses         = max_uses.to_i
      @uses             = 0
    end

    def increment_uses!
      @uses += 1
    end

    def valid?
      @uses < @max_uses
    end

    def expired?
      Date.today > @expiration_date
    end

    def active?
      valid? && !expired?
    end

  end
end
