class Ecommerce
  class Model
    class << self

      attr_accessor :items

      def load_data(file_path)
        @items = []
        CSV.foreach(file_path) do |row|
          @items << self.new(*row) if row.any?
        end
      end

      def all
        @items
      end

    end
  end
end
