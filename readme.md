Modo de uso:
---

Instalar dependências:

```
bundle install
```

Execução:
---

Altere a permissão do arquivo para *executável* e execute o script:

```
chmod +x exercicio
./exercicio data/coupons.csv data/products.csv data/orders.csv data/order_items.csv data/totals.csv
```

Teste o código:

```
bundle exec rspec
```

Um arquivo HTML com o code coverage será gerado após o teste e ficará disponível para análise em:

```
coverage/index.html
```
