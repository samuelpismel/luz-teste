require 'spec_helper'

describe Ecommerce::Model do

  # should be used only as super class
  let(:model) { Ecommerce::Product }

  describe ".load_data" do
    it "should load data from file to @items" do
      model.load_data 'data/products.csv'

      expect(model.items).not_to be nil
      expect(model.items.first.class).to eq model
    end
  end

  describe ".all" do
    it "should return @items" do
      model.load_data 'data/products.csv'

      expect(model.all).to eq model.items
    end
  end

end
