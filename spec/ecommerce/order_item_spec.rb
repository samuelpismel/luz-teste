require 'spec_helper'

require 'csv'

require 'ecommerce/model'
require 'ecommerce/product'
require 'ecommerce/order_item'

describe Ecommerce::OrderItem do
  let(:order_item) { Ecommerce::OrderItem.new(123, 789) }

  describe '#initialize' do
    it '@order_id should be an Fixnum' do
      expect(order_item.order_id.class).to be Fixnum
    end
    it '@order_id should be an Fixnum' do
      expect(order_item.order_id.class).to be Fixnum
    end
  end

  describe '#product' do
    it 'should return the order_item product' do
      Ecommerce::Product.load_data 'data/products.csv'
      valid_product = Ecommerce::Product.all.delete_if { |product| product.id != order_item.product_id }.first
      expect(order_item.product).to be valid_product
    end
  end

end
