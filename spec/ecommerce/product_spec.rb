require 'spec_helper'

require 'ecommerce/model'
require 'ecommerce/product'

describe Ecommerce::Product do
  let(:product) { Ecommerce::Product.new(123, 150) }

  describe '#initialize' do

    it '@id should be an Fixnum' do
      expect(product.id.class).to be Fixnum
    end
    it '@price should be an Fixnum' do
      expect(product.price.class).to be Float
    end

  end

end
