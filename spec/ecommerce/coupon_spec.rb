require 'spec_helper'

require 'ecommerce/model'
require 'ecommerce/coupon'

describe Ecommerce::Coupon do
  let(:coupon) { Ecommerce::Coupon.new(123, 25, 'absolute', '2020/12/25', 1) }

  describe '#initialize' do

    it '@id should be an Fixnum' do
      expect(coupon.id.class).to be Fixnum
    end
    it '@discount should be an Float' do
      expect(coupon.discount.class).to be Float
    end
    it '@discount_type should be an String' do
      expect(coupon.discount_type.class).to be String
    end
    it '@expiration_date should be an Date' do
      expect(coupon.expiration_date.class).to be Date
    end
    it '@max_uses should be an Fixnum' do
      expect(coupon.max_uses.class).to be Fixnum
    end
    it '@uses should be an Fixnum' do
      expect(coupon.uses.class).to be Fixnum
    end
    it '@uses should be 0' do
      expect(coupon.uses).to eql 0
    end

  end

  describe '#increment_uses!' do

    context 'when @uses is 0' do
      it 'should return 1' do
        coupon.increment_uses!
        expect(coupon.uses).to eql 1
      end
    end

  end

  describe '#valid?' do

    context 'when @uses is less than @max_uses' do
      it 'should return true' do
        expect(coupon.valid?).to be true
      end
    end

    context 'when @uses is equal to @max_uses' do
      it 'should return false' do
        coupon.increment_uses!
        expect(coupon.valid?).to be false
      end
    end

    context 'when @uses is greather than @max_uses' do
      it 'should return false' do
        2.times { coupon.increment_uses! }
        expect(coupon.valid?).to be false
      end
    end

  end

  describe '#expired?' do

    context 'when current date is less than @expiration_date' do
      it 'should be false' do
        expect(coupon.expired?).to be false
      end
    end

    context 'when current date is more than @expiration_date' do
      it 'should be true' do
        coupon = Ecommerce::Coupon.new(123, 25, 'absolute', '2000/01/01', 1)
        expect(coupon.expired?).to be true
      end
    end

  end

  describe '#active?' do

    context 'when coupon is not valid' do
      it 'should be false' do
        2.times { coupon.increment_uses! }
        expect(coupon.active?).to be false
      end
    end

    context 'when coupon is expired' do
      it 'should be false' do
        coupon = Ecommerce::Coupon.new(123, 25, 'absolute', '2000/01/01', 1)
        expect(coupon.active?).to be false
      end
    end

    context 'when coupon is valid and is not expired' do
      it 'should be true' do
        expect(coupon.active?).to be true
      end
    end

  end

end
