require 'spec_helper'

require 'csv'

require 'ecommerce'
require 'ecommerce/model'
require 'ecommerce/coupon'
require 'ecommerce/order'
require 'ecommerce/order_item'
require 'ecommerce/product'

describe Ecommerce::Order do

  before do
    # simulate command line arguments
    ARGV[0] = 'data/coupons.csv'
    ARGV[1] = 'data/products.csv'
    ARGV[2] = 'data/orders.csv'
    ARGV[3] = 'data/order_items.csv'

    Ecommerce.send :load_data!
  end

  let(:order) { Ecommerce::Order.new(234, 123) }

  describe '#initialize' do
    it '@id should be an Fixnum' do
      expect(order.id.class).to be Fixnum
    end
    it '@coupon_id should be an Fixnum' do
      expect(order.coupon_id.class).to be Fixnum
    end
    it '@items should be an Array' do
      expect(order.items.class).to be Array
    end
    it '@products should be an Array' do
      expect(order.products.class).to be Array
    end
    it '@sub_total should be an Fixnum' do
      expect(order.sub_total.class).to be Fixnum
    end
    it '@sub_total should be 0' do
      expect(order.sub_total).to eq 0
    end
    it '@discount should be an Fixnum' do
      expect(order.discount.class).to be Fixnum
    end
    it '@discount should be 0' do
      expect(order.discount).to eq 0
    end
    it '@total should be an Fixnum' do
      expect(order.total.class).to be Fixnum
    end
    it '@total should be 0' do
      expect(order.total).to eq 0
    end
  end

  describe '#load_coupon' do
    it 'should assign @coupon with the order coupon' do
      order.send :load_coupon
      right_coupon = Ecommerce::Coupon.all.delete_if{ |coupon| coupon.id != order.coupon_id }.first

      expect(order.instance_variable_get(:@coupon)).not_to be nil
      expect(order.coupon).to be right_coupon
    end
  end

  describe '#load_items' do
    it 'should assign @items with the order OrderItems' do
      order.send :load_items
      right_items = Ecommerce::OrderItem.all.delete_if{ |order_item| order_item.order_id != order.id }

      expect(order.items).not_to eq []
      expect(order.items).to eq right_items
    end
  end

  describe '#load_products' do
    it 'should assign @items with the order products' do
      order.send :load_items
      order.send :load_products

      items_products_ids = order.items.map(&:product_id)

      right_products = Ecommerce::Product.all.delete_if do |product|
        !items_products_ids.include? product.id
      end

      expect(order.products).not_to eq []
      expect(order.products).to eq right_products
    end
  end

  describe "#calculate_coupon_discount" do
    context "when @coupon is not set" do
      it "should return zero" do
        order.instance_variable_set :@coupon, nil
        expect(order.send(:calculate_coupon_discount)).to eq 0
      end
    end
    context "when @coupon is invalid" do
      it "should return zero" do
        coupon = Ecommerce::Coupon.new(123, 25, 'absolute', '2000/01/01', 1)
        order.instance_variable_set :@coupon, coupon
        expect(order.send(:calculate_coupon_discount)).to eq 0
      end
    end
    context "when discount_type is 'absolute'" do
      it "should return the coupon value" do
        coupon = Ecommerce::Coupon.new(123, 25, 'absolute', '2020/12/25', 1)
        order.instance_variable_set :@coupon, coupon
        expect(order.send(:calculate_coupon_discount)).to eq 25
      end
    end
    context "when discount_type is 'percent'" do
      it "should return the percentage of subtotal" do
        coupon = Ecommerce::Coupon.new(234, 15, 'percent', '2020/01/01', 2)
        order.instance_variable_set :@coupon, coupon
        order.instance_variable_set :@sub_total, 100

        expect(order.send(:calculate_coupon_discount)).to eq 15
      end
    end
  end

  describe "#calculate_progressive_discount" do
    it "should return 0 for just one product" do
      order.instance_variable_set :@products, [Ecommerce::Product.new(1, 1)]
      expect(order.send(:calculate_progressive_discount)).to eq 0
    end
    it "should return 0.1 for two products" do
      products = []
      2.times { |i| products << Ecommerce::Product.new(i, i) }
      order.instance_variable_set :@products, products
      order.instance_variable_set :@sub_total, 1

      expect(order.send(:calculate_progressive_discount)).to eq 0.1
    end
    it "should return 0.2 for four products" do
      products = []
      4.times { |i| products << Ecommerce::Product.new(i, i) }
      order.instance_variable_set :@products, products
      order.instance_variable_set :@sub_total, 1

      expect(order.send(:calculate_progressive_discount)).to eq 0.20
    end
    it "should return 0.4 for eight products" do
      products = []
      8.times { |i| products << Ecommerce::Product.new(i, i) }
      order.instance_variable_set :@products, products
      order.instance_variable_set :@sub_total, 1

      expect(order.send(:calculate_progressive_discount)).to eq 0.4
    end
    it "should return 0.4 for ten products" do
      products = []
      10.times { |i| products << Ecommerce::Product.new(i, i) }
      order.instance_variable_set :@products, products
      order.instance_variable_set :@sub_total, 1

      expect(order.send(:calculate_progressive_discount)).to eq 0.4
    end
  end

  describe "#apply_discount" do
    it "should apply the greater discount between coupon or progressive" do
      coupon = Ecommerce::Coupon.new(234, 50, 'percent', '2020/01/01', 2)
      order.instance_variable_set :@coupon, coupon

      products = []
      10.times { |i| products << Ecommerce::Product.new(i, i) }
      order.instance_variable_set :@products, products

      order.instance_variable_set :@sub_total, 100
      order.send :apply_discount

      expect(order.discount).to eq 50
    end
  end


  describe '#sum_subtotal' do
    it 'should be the sum of the products prices' do
      products = []
      10.times { |i| products << Ecommerce::Product.new(i, i) }

      order.instance_variable_set :@products, products
      order.send :sum_subtotal

      expect(order.sub_total).to eq 45.0
    end
  end

  describe '#sum_total' do
    it 'should be the diference of @sub_total and @discount' do
      order.instance_variable_set :@sub_total, 100
      order.instance_variable_set :@discount, 10
      order.send :sum_total
      expect(order.total).to eq 90
    end
  end

  describe "#process!" do
    it "should load and process the order" do
      order.process!
      expect(order.coupon).not_to be nil
      expect(order.items).not_to be nil
      expect(order.products).not_to be nil
      expect(order.sub_total).not_to be nil
      expect(order.discount).not_to be nil
      expect(order.total).not_to be nil
    end
  end

end
