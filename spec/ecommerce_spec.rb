require 'spec_helper'

require 'csv'

require 'ecommerce'
require 'ecommerce/model'
require 'ecommerce/coupon'
require 'ecommerce/order'
require 'ecommerce/order_item'
require 'ecommerce/product'

describe Ecommerce do

  before do
    # simulate command line arguments
    ARGV[0] = 'data/coupons.csv'
    ARGV[1] = 'data/products.csv'
    ARGV[2] = 'data/orders.csv'
    ARGV[3] = 'data/order_items.csv'
    ARGV[4] = 'tmp/totals.csv'
  end

  describe '.process!' do
    it "should process all orders and write the right output" do
      Ecommerce.process!

      output_data = []
      CSV.foreach('data/output.csv') do |row|
        output_data << [row[0].to_i, row[1].to_f]
      end

      totals_data = []
      CSV.foreach(ARGV[4]) do |row|
        totals_data << [row[0].to_i, row[1].to_f]
      end

      expect(totals_data).to eq output_data
    end
  end

  describe '.load_data!' do
    it "should load data from files paths passed as arguments" do
      Ecommerce.send :load_data!

      expect(Ecommerce::Coupon.all).not_to be []
      expect(Ecommerce::Product.all).not_to be []
      expect(Ecommerce::Order.all).not_to be []
      expect(Ecommerce::OrderItem.all).not_to be []
    end
  end

  describe '.process_orders!' do
    it "should process all orders" do
      raw_orders = Ecommerce::Order.all.clone
      Ecommerce.send :process_orders!
      final_orders = Ecommerce::Order.all.clone

      expect(final_orders).not_to be raw_orders
    end
  end

  describe '.write_output!' do
    it "should write the orders totals in a file" do
      Ecommerce.process!
      expect(File.exists?(ARGV[4])).to be true
    end
  end

end
